﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankApi.IServices;
using BankApi.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankApi.Controllers
{
    //[Authorize]
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        //Authenticate part
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]UserCredential userCredential)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var user = await _userService.Authenticate(userCredential.Username, userCredential.Password);
            if (user == null)
            {
                return BadRequest("Username or password is incorrect");
            }
            return Ok(user);
        }
        //Get all users
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _userService.GetUsers();
            return Ok(users);
        }
        //Get user by Id
        [Route("{Id}")]
        [HttpGet]
        public async Task<IActionResult> GetUserById(string Id)
        {
            var user = await _userService.GetUserById(Id);
            return Ok(user);
        }
        //Add new user
        [HttpPost]
        public async Task<IActionResult> AddUser([FromBody]User User)
        {
            var newProduct = await _userService.AddUser(User);
            return Ok(newProduct);
        }

        //Delete user
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteUser(string Id)
        {
            var deleteuser = await _userService.DeleteUser(Id);
            return Ok(deleteuser);
        }
        //UPdate user
        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody]User User)
        {
            var updateduser = await _userService.UpdateUser(User);
            if (updateduser == false)
            {
                return NotFound("No user found with given id");
            }
            return Ok("Successfully updated");
        }
    }
}