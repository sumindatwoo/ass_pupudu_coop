﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankApi.IRepositories;
using BankApi.IServices;
using BankApi.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankApi.Controllers
{  //[Authorize]
    [Route("api/accounts")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        //get All account
        [HttpGet]
        public async Task<IActionResult> GetUserAccounts()
        {
            var acc = await _accountService.GetUserAccounts();
            return Ok(acc);
        }
        //get acount by Id
        [Route("{Id}")]
        [HttpGet]
        public async Task<IActionResult> GetAccountByIdnum(string Id)
        {
            var Acc = await _accountService.GetAccountByIdnum(Id);
            if (Acc != null)
            {
                return Ok(Acc);
            }
            else
            {
                return NotFound("Account not found");
            }
        }

        //Add new account
        [HttpPost]
        public async Task<IActionResult> AddAccountUser([FromBody]Account account)
        {
            var newProduct = await _accountService.AddAccountUser(account);
            return Ok(newProduct);
        }
        //[Authorize(Policy = "AdminOnly")]
        //Delete account
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAccountUser(string Id)
        {
            var accDeleted = await _accountService.DeleteAccountUser(Id);
            if (accDeleted == false)
            {
                return NotFound("No account found ");
            }
            return Ok(accDeleted);
        }
        //update account
        [HttpPut]
        public async Task<IActionResult> UpdateAccountUser([FromBody]Account account)
        {
            var updatedaccount = await _accountService.UpdateAccountUser(account);
            if (updatedaccount == false)
            {
                return NotFound("No account found");
            }
            return Ok(updatedaccount);
        }

    }
}