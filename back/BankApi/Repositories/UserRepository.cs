﻿using BankApi.IRepositories;
using BankApi.Model;
using BankApi.Oracle;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BankApi.Repositories
{
    public class UserRepository : IUserRepository
    {
        private IConfiguration configuration;

        public UserRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }
        public async Task<bool> AddUser(User user)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ID_", OracleDbType.Int32, ParameterDirection.Input, user.Id);
                dyParam.Add("NAME_", OracleDbType.Varchar2, ParameterDirection.Input, user.Name);
                dyParam.Add("ROLE_", OracleDbType.Varchar2, ParameterDirection.Input, user.Role);
                dyParam.Add("PASSWORD_", OracleDbType.Varchar2, ParameterDirection.Input, user.Password);
                dyParam.Add("USERNAME_", OracleDbType.Varchar2, ParameterDirection.Input, user.UserName);
                

                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_USR_ADD";
                    await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            User result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("USERNAME_", OracleDbType.Varchar2, ParameterDirection.Input, username);
                dyParam.Add("PASSWORD_", OracleDbType.Varchar2, ParameterDirection.Input, password);
                dyParam.Add("USER_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_LOGIN";
                    result = (await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (result == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING");
            var tokenDescripter = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.Name,result.Id.ToString()),
                    new Claim(ClaimTypes.Role, result.Role)
                }),

                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)

            };

            var token = tokenHandler.CreateToken(tokenDescripter);
            result.Token = tokenHandler.WriteToken(token);
            result.Password = null;

            return result;
        }

        public async Task<bool> DeleteUser(string Id)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("Id_", OracleDbType.Int32, ParameterDirection.Input, Id);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_USER_DELETE";
                    (await SqlMapper.QueryAsync(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }

        public async Task<User> GetUserById(string Id)
        {
            User result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("Id_", OracleDbType.Int32, ParameterDirection.Input, Id);
                dyParam.Add("US_DETAILS_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANAK_USR_GET_ID";
                    result = (await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }

        public async Task<List<User>> GetUsers()
        {
            List<User> result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("USRCURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_USR_GET_ALL";
                    result = (await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure)).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }

        public async Task<bool> UpdateUser(User user)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ID_", OracleDbType.Int32, ParameterDirection.Input, user.Id);
                dyParam.Add("NAME_", OracleDbType.Varchar2, ParameterDirection.Input, user.Name);
                dyParam.Add("ROLE_", OracleDbType.Varchar2, ParameterDirection.Input, user.Role);
                dyParam.Add("PASSWORD_", OracleDbType.Varchar2, ParameterDirection.Input, user.Password);
                dyParam.Add("USERNAME_", OracleDbType.Varchar2, ParameterDirection.Input, user.UserName);


                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_USR_UPDATE";
                    await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }

        public IDbConnection GetConnection()
        {
            var connectionString = configuration.GetSection("ConnectionStrings").GetSection("AccountConnection").Value;
            var conn = new OracleConnection(connectionString);
            return conn;
        }
    }
}
