﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.Handlers.AuthorizationHandlers
{
    public class RoleRequirment : IAuthorizationRequirement
    {    
            public RoleRequirment(string role)
            {
                this.Role = role;
            }
            public string Role { get; }      
    }
}

