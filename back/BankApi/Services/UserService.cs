﻿using BankApi.IRepositories;
using BankApi.IServices;
using BankApi.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.Services
{
    public class UserService : IUserService
    {
        IConfiguration configuration;
        IUserRepository userRepository;

        public UserService(IConfiguration _configuration, IUserRepository _userRepository)
        {
            configuration = _configuration;
            userRepository = _userRepository;
        }
        public async Task<bool> AddUser(User user)
        {
            var adduser = await userRepository.AddUser(user);
            return adduser;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            var loguser = await userRepository.Authenticate(username, password);
            return loguser;
        }

        public async Task<bool> DeleteUser(string Id)
        {
            var deluser = await userRepository.DeleteUser(Id);
            return deluser;
        }

        public async Task<User> GetUserById(string Id)
        {
            var getuserid = await userRepository.GetUserById(Id);
            return getuserid;
        }

        public async Task<List<User>> GetUsers()
        {
            var getusers = await userRepository.GetUsers();
            return getusers;
        }

        public async Task<bool> UpdateUser(User user)
        {
            var updateusers = await userRepository.UpdateUser(user);
            return updateusers;
        }
    }
}
