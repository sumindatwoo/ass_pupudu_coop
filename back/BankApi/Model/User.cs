﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.Model
{
    public class User
    {
        [Required(ErrorMessage = "ID is requird")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is requird")]
        public string Name { get; set; }

        [Required(ErrorMessage = "UserName is requird")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is requird")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Role is requird")]
        public string Role { get; set; }

        public string Token { get; set; }
    }
}
