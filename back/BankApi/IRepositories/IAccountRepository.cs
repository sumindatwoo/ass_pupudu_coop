﻿using BankApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.IRepositories
{
    public interface IAccountRepository
    {
        Task<List<Account>> GetUserAccounts();
        Task<Account> GetAccountByIdnum(string Id);
        Task<bool> AddAccountUser(Account account);
        Task<bool> UpdateAccountUser(Account account);
        Task<bool> DeleteAccountUser(string Id);
    }
}
