import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Observable, from } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private authSrvice: AuthService, public router: Router) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone(
            {
                setHeaders: {
                    Authorization: `Bearer ${this.authSrvice.getToken()}`
                }
            }
        );

        return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                //response wena dewal
            }
        }, (err: any) => {
            if (err instanceof HttpResponse) {
                if (err.status == 401) {
                    localStorage.removeItem("auth");
                    this.router.navigate(['profile']);
                }
            }
        }

        ))
    }

}