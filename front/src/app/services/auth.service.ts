import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable()
export class AuthService {
  context: string = "users"
  constructor(private httpClient: HttpClient) {

  }

  Login(userDate: Login) {
    return this.httpClient.
      post<Token>(`${environment.BASE_URL}${this.context}/authenticate`, userDate)
      .toPromise()
      .then(res => {
        localStorage.setItem("auth", res.token);
        localStorage.setItem("role", res.role);

        return true;
      }).catch(err => {
        console.log(err);
        return false;
      })
  }

  public Logout(){
    localStorage.removeItem("auth");
    localStorage.removeItem("role");

  }

  public getToken(): string {
    return localStorage.getItem("auth");
  }
  public getRole(): string {
    return localStorage.getItem("role");
  }

}