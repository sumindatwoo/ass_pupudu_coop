import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private accounts: Account2[] = []
  context: string = "accounts"  

  constructor(private httpClient: HttpClient ) { }

  

  getAccountsDB():Observable<Account2[]>{
    return this.httpClient.get<Account2[]>(environment.BASE_URL + this.context)
  }

  getAccountByIDDB(id):Observable<Account2>{
    return this.httpClient.get<Account2>(environment.BASE_URL + this.context +"/" + id);
  }

  public deleteAccountByIDDB(id):Observable<boolean>{
    return this.httpClient.delete<boolean>(environment.BASE_URL + this.context +"/" + id);
  }

  newAccountDB(p:Account2):Observable<boolean>{
    return this.httpClient.post<boolean>(environment.BASE_URL + this.context,p);
  }

  updateAccountDB(p:Account2):Observable<boolean>{
    return this.httpClient.put<boolean>(environment.BASE_URL + this.context,p);
  }

}




