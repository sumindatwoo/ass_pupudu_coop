import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { AuthService } from '../services/auth.service';
import {ActivatedRoute} from "@angular/router";
import {Router} from "@angular/router";
import { AccountComponent } from "../account/account.component";

@Component({
  providers:[AccountComponent],
  selector: 'app-p-form',
  templateUrl: './p-form.component.html',
  styleUrls: ['./p-form.component.css']
})
export class PFormComponent implements OnInit {
  newAccount :Account2= {
    id : "",
    name : "",
    balance:"",
    branch:"",
    owner:""
  }
  updating : Boolean;
  role : string;

  constructor(
    private AccountService:AccountService, 
    private router: Router,
    private route: ActivatedRoute,
    private AuthService:AuthService,
    private AccountComponent:AccountComponent
    ) { }

  ngOnInit() {
    this.route.params.subscribe( params => this.newAccount.id = params['id'] );

    this.updating = (this.newAccount.id==undefined)? false : true;

    if(this.updating)
      this.AccountService.getAccountByIDDB(this.newAccount.id).subscribe(res=> this.newAccount=res,err=> console.log(err));
  
    
    this.loginCheck();

  }

  loginCheck(){
    let token = this.AuthService.getToken();
    if(token=='' || token==undefined || token==null) {
      this.router.navigate(['/login']);
    }
  }
  
  
  Submit(){
    let isManager = (this.AuthService.getRole() =='Manager') ? true : false;
    if(!isManager && this.updating)
      alert('You can update you are not a manager')

    if(!this.updating){
      this.AccountService.newAccountDB(this.newAccount).subscribe(res=>{
        alert("New Account Added")
        this.router.navigate(['/Account']);
      },(err)=>{console.log(err)});
    }
    else if (this.updating && isManager) {
      this.AccountService.updateAccountDB(this.newAccount).subscribe(res=>{
        alert(" Account Updated")
        this.router.navigate(['/Account']);
        this.AccountComponent.updateSuccess();
      },(err)=>{console.log(err)});
    }
    
  }

  update(){
    this.AccountService.updateAccountDB(this.newAccount).subscribe((res)=>{
      alert("ID "+this.newAccount.id +" Account Updated");
      this.router.navigate(['/Account']);
    },(err)=>{console.log(err)});
  }

 delete(){
    this.AccountService.deleteAccountByIDDB(this.newAccount.id).subscribe(res=>{
      alert("ID "+this.newAccount.id +" Delete Account");
      this.router.navigate(['/Account']);
      this.AccountComponent.deleteSuccess();
    },(err)=>{console.log(err)});
  }

}
