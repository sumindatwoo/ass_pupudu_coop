import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  accounts: Account2[];
  newAccount :Account2={
    id :"",
    name : "",
    balance : "",
    branch : "",
    owner:""
  }
  
  selectAccount:Account2={
    id :"",
    name : "",
    balance : "",
    branch : "",
    owner:""
  }
  constructor(
    private router:Router,
    private AccountService:AccountService,
    private AuthService:AuthService,
    private _flashMessagesService: FlashMessagesService
    ) { 

  }
  

  ngOnInit() {

    this.AccountService.getAccountsDB().subscribe((res)=>{
      this.accounts=res;
      console.log(this.accounts);

    },(err)=>{
      console.log(err);
    });
    this.loginCheck();
  }

  loginCheck(){
    let token = this.AuthService.getToken();
    if(token=='' || token==undefined || token==null) {
      this.router.navigate(['/login']);
    }
  }

  AccForm(){
    this.router.navigate(['p_form']);
  }

  delete(i){
    if(confirm("Are you sure do you want to delete ? ")){
      this.AccountService.deleteAccountByIDDB(this.accounts[i].id).subscribe(res=>{
        // alert("ID "+this.accounts[i].id +" Delete Account");
        this._flashMessagesService.show("ID "+this.accounts[i].id +" Delete Account", { cssClass: 'alert-success', timeout: 3000 });
        this.accounts.splice(i,1);
      this.router.navigate(['/Account']);
    },(err)=>{console.log(err)});
    }
    
  }
  update(i){
    this.router.navigate(['/p_form',this.accounts[i].id]);
  }
    
  view(i){
    this.AccountService.getAccountByIDDB(this.accounts[i].id).subscribe(res=>{   
      this.selectAccount = res;
      console.log(res);
  },(err)=>{console.log(err)});
  }
  updateSuccess(){
    this._flashMessagesService.show("Update Success", { cssClass: 'alert-info', timeout: 5000 });   
  }
  deleteSuccess(){
    this._flashMessagesService.show("Delete Success", { cssClass: 'alert-danger', timeout: 5000 });   
  }

}
